import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

import Moment from 'react-moment';

export const PracticeTable = (props) => {
  return (
    <Table responsive bordered hover size="sm" variant="dark">
      <thead>
        <tr>
          <th>Practice Type</th>
          <th>Start</th>
          <th>Finish</th>
          <th>Duration</th>
          <th>Remove</th>
        </tr>
      </thead>
      <tbody>
        {
          props.practiceEvents.map((practiceEvent) =>
              <PracticeEventRow
                key={practiceEvent.id}
                practiceEvent={practiceEvent}
                handleRemovePracticeEvent={props.handleRemovePracticeEvent}
              />
          )
        }
      </tbody>
    </Table>
  );
}

const PracticeEventRow = (props) => {
  const start = new Date(props.practiceEvent.start);
  const finish = new Date(props.practiceEvent.finish);
  const handleDeleteClick = () => {
    props.handleRemovePracticeEvent(props.practiceEvent.id);
  };
  return (
    <tr>
      <td>{props.practiceEvent.name}</td>
      <td><Moment format="YYYY-MM-DD HH:mm:ss">{start}</Moment></td>
      <td><Moment format="YYYY-MM-DD HH:mm:ss">{finish}</Moment></td>
      <td><Moment format="m [m] ss [sec]">{finish - start}</Moment></td>
      <td><Button onClick={handleDeleteClick}>X</Button></td>
    </tr>
  );
}