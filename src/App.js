import React from 'react';
import './App.css';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

import { PracticeTable } from './components/PracticeTable';
import { Timer1 } from './utils';

import uuidv4 from 'uuid/v4';

import moment from 'moment';


class PracticeEvents extends React.Component {

  constructor(props) {
    super(props);
    this.handleStartPractice = this.handleStartPractice.bind(this);
    this.handleFinishPractice = this.handleFinishPractice.bind(this);
    this.handleRemovePracticeEvent = this.handleRemovePracticeEvent.bind(this);
    this.messagesEnd = undefined;
    this.state = {
      practiceEvents: [],
      newPractice: undefined,
      startTime: undefined,
    };
  }

  scrollToBottom() {
    // Learned about from https://stackoverflow.com/questions/37620694/how-to-scroll-to-bottom-in-react
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    console.log('SCROLL');
  }

  handleStartPractice(e) {
    e.preventDefault();
    const practice = e.target.elements.practice.value;

    this.setState(() => {
      return {
        newPractice: practice,
        startTime: new Date(),
      };
    });
  }

  handleFinishPractice(e) {
    e.preventDefault();

    const updatedPracticeEvents = this.state.practiceEvents.slice()
    const newId = uuidv4();
    updatedPracticeEvents.push({
      id: newId,
      name: this.state.newPractice,
      start: this.state.startTime,
      finish: new Date(),
    });
    console.log("this.state.practiceEvents after update", updatedPracticeEvents);
    this.setState(() => {
      return {
        practiceEvents: updatedPracticeEvents,
        startTime: undefined,
        newPractice: undefined,
      };
    });
  }

  handleRemovePracticeEvent(id) {
    // This is where I want to remove an event if the button has been clicked.
    console.log('Remove', id);

    this.setState(() => ({
      practiceEvents: this.state.practiceEvents.filter((practiceEvent) => {
        return practiceEvent.id !== id;
      })
    }));
  }

  componentDidMount() {
    console.log('componentDidMount')

    // Going to start by saving data out to the localStorage
    const json = localStorage.getItem('practiceEvents');
    if (json) {
      const practiceEvents = JSON.parse(json);
      this.setState(() => ({ practiceEvents: practiceEvents }));
      console.log('loaded form localstorage');
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate', prevState.practiceEvents.length, this.state.practiceEvents.length);
    if (prevState.practiceEvents.length !== this.state.practiceEvents.length) {
      const json = JSON.stringify(this.state.practiceEvents);
      localStorage.setItem('practiceEvents', json);
      console.log('Saved practiceEvents');
    }

    // We want to scroll conditionally - preferrable only when the user
    // is creating new records or starting a new one.
    // if they are deleting records we will not scroll.
    if (prevState.practiceEvents.length <= this.state.practiceEvents.length) {
      this.scrollToBottom();
    }
  }

  render() {
    return (
      <div>
      <Container>
        <Col>
          <PracticeTable
            practiceEvents={this.state.practiceEvents}
            handleRemovePracticeEvent={this.handleRemovePracticeEvent}
          />

          <Card body className="bg-dark text-white">
            {!this.state.startTime &&
            <Form onSubmit={this.handleStartPractice}>
              <Form.Control type="text" name="practice" placeholder="Practice Type" autoComplete="off"/>
              <Button type="submit" variant="primary" block>START</Button>
            </Form>
            }
            {this.state.startTime &&
              <Form onSubmit={this.handleFinishPractice}>
                <Form.Control
                  type="text"
                  name="practice"
                  placeholder="Practice Type"
                  value={this.state.newPractice}
                  readOnly={true}
                />
                <Form.Control
                  type="text"
                  value={moment(this.state.startTime).format("YYYY-MM-DD HH:mm:ss")}
                  readOnly={true}
                />
                <Timer1 startTime={this.state.startTime}/>
                <Button type="submit" variant="primary" block>FINISH</Button>
              </Form>
            }
            <div
              id="foobar"
              style={{ float:"left", clear: "both" }}
              ref={(el) => { this.messagesEnd = el; }}>
            </div>
          </Card>
          <br/>
        </Col>
      </Container>
      </div>
    );
  }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Practice Events</h2>
        <PracticeEvents />
      </header>
    </div>
  );
}

export default App;
