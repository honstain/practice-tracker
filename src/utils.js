import React from 'react';
import timer from 'react-timer-hoc';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

momentDurationFormatSetup(moment);

class CountDownComponent extends React.Component {
  constructor(props) {
    super(props);
    this.calculateAndFormatTimeDiff = this.calculateAndFormatTimeDiff.bind(this);
  }
  calculateAndFormatTimeDiff() {
    // Took a dependency on a non-official momentJS plugin
    // https://github.com/jsmreese/moment-duration-format
    const duration = moment.duration(moment(Date.now()).diff(moment(this.props.startTime)));
    return duration.format('mm[m] ss[s]', { trim: false })
  }
  render() {
    return (
      <div>
        <p>{this.calculateAndFormatTimeDiff()}</p>
      </div>
    );
  }
}

const Timer1 = timer(1000)(CountDownComponent);

export { CountDownComponent, Timer1 }